<?php
require "vendor/autoload.php";
function getHtml($url, $post = null) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    if(!empty($post)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization: Basic '.base64_encode("api:key-4a5f9411e579945047342ba09642b495")
            ));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}


function sendEmail( $to, $subject, $content)
{
    return getHtml(
        "https://api.mailgun.net/v3/mg.paynura.com/messages",
        http_build_query(
            array(
                'from' => 'noreply@mg.paynura.com',
                'to' => $to,
                'subject' => $subject,
                'html' => $content
            )
        )
    );
}

$name = $_POST["name"];
$email = $_POST["email"];
$phone = $_POST["accountid"];
$message = $_POST["accountid_friend"];
$mode = $_POST["mode"];

$template = file_get_contents(__DIR__."/paynura_cashback_register.html");
$m = new Mustache_Engine(array('entity_flags' => ENT_QUOTES));
$render= $m->render($template, array('name' => $name)); // "Hello World!"

#Send it to us
sendEmail(
    array("dejvid.kit@gmail.com", "dejvid@snet.si", "manager@paynura.com"),
    "Paynura Cashout Request Form submitted from ".$email ,
    "<ul><li>Name: ".$name."</li></ul>".
            "<ul><li>Email: ".$email."</li></ul>".
            "<ul><li>AccountID: ".$phone."</li></ul>".
            "<ul><li>Friend Account ID: ".$message."</li></ul>".
            "<ul><li>Backend: ".$mode."</li></ul>"
);

sendEmail(
    array($email),
    "Registration at Paynura Cashback Program" ,
    $render
);
